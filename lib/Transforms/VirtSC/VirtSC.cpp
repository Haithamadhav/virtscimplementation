//===- VirtSC.cpp ---------------===//
// Modified from Hello pass
//
//
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "VirtSC.def"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/ValueSymbolTable.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/IR/IRBuilder.h"
#include <map> 
#include <cstdlib>
using namespace llvm;

#define DEBUG_TYPE "VirtSC"

#define NUM_FUNS  30
#define INST_PER_FUN 50
#define NUM_RISA 60
#define NUM_OPERANDS 3 
#define NUM_BB_PER_FUN 10
namespace {
	class VirtSC : public ModulePass{
	public:
		static char ID;
		VirtSC() : ModulePass(ID) {}
		~VirtSC() {}
		StringRef Funct[NUM_FUNS];
		StringRef BBNames[NUM_FUNS][NUM_BB_PER_FUN];
		int BBCountInFun[NUM_FUNS];
		int insInFunct[NUM_FUNS];
	//	StringRef RISA[NUM_RISA];
		//int VPAS[NUM_FUNS][INST_PER_FUN];
		int *VPAS[NUM_FUNS];//UK
		Type* FRetType[NUM_FUNS];
		FunctionType* FType[NUM_FUNS];
		Module* mod;
		std::map<int,int> RISA;
		std::map<int,int> revRISA;
		int random;
		LLVMContext *C;
		struct VMSS{
			Value* Operands[INST_PER_FUN][NUM_OPERANDS];
			Type* opType[INST_PER_FUN];
			StringRef nameLabel[INST_PER_FUN];
			StringRef operandLabels[INST_PER_FUN][NUM_OPERANDS];
		};//VM[NUM_FUNS];
		VMSS* VM;
		int funcCounter;
		int instCounter;
    		virtual bool runOnModule(Module &M) {
			C=&(M.getContext());
			srand(5);
			int funcs = getFunctionCount(M);
			VM=(VMSS*)calloc(funcs,sizeof(VMSS));
			funcCounter=0;//initialized because value was random after calloc(previous) instruction
			M.dump();
			for(Module::iterator MI=M.begin(); MI !=M.end(); ++MI){
				Function &F(*MI);
				errs() << "\nFunction :..... "<<F.getName()<<".......\n\n";
				bool status;
				status = virtualizeFunction(F); //This function iterates through each instruction and generate opcode to store in VPA.
			}
			globalFunct();//only for printing and checking, no logical use
			interpreter();
			for(int i=0;i<funcCounter;i++){
				free(VPAS[i]);//UK
			}
			free(VM);
			return true;
	    	}
		int generateRandOpcode(Instruction &inst) {
			random = rand();
			RISA.insert(std::pair<int,int>(inst.getOpcode(), random));
			revRISA.insert(std::pair<int,int>(random,inst.getOpcode()));
			return random;
		}
		unsigned getFunctionCount(Module &M) {
			int count =0;
			for(Module::iterator MI=M.begin(); MI !=M.end(); ++MI)
			{
				count++;
			}
			return count;
		}
		unsigned getInstructionCount(Function &F) {//UK
			int count =0;	
			for(Function::iterator FI=F.begin();FI != F.end(); ++FI) {
				BasicBlock &BB(*FI);
				for (BasicBlock::iterator BBI = BB.begin(); BBI != BB.end(); ++BBI) {
					count ++;
				}
			}
			return count;
		}
		bool virtualizeFunction(Function &F) {
			errs()<<"Entered function............"<<F.getName()<<"..........."<<funcCounter<<"...\n";
			Funct[funcCounter]=F.getName();
//			F.dump();
			FRetType[funcCounter]=F.getReturnType();
			FType[funcCounter]=F.getFunctionType();			
			instCounter = 0;
			int bbCounter = 0;
			int ins = getInstructionCount(F);//UK
			insInFunct[funcCounter]=ins; 
			VPAS[funcCounter] = (int *)malloc(ins*sizeof(int));//UK
			for(int i=0;i<ins;i++){
				VPAS[funcCounter][i]=0;
			}
			for(Function::iterator FI=F.begin();FI != F.end(); ++FI) {
				BasicBlock &BB(*FI);
				BBNames[funcCounter][bbCounter]=BB.getName();
				for (BasicBlock::iterator BBI = BB.begin(); BBI != BB.end(); ++BBI) {
     					 Instruction &inst(*BBI); 
					 VPAS[funcCounter][instCounter]=generateRandOpcode(inst);//make this  a function
					 if (inst.isTerminator()) {
						 storeTerminatorOperands(inst); //function to handle operands
					 } else if (inst.isBinaryOp()) {
						 storeBinOperands(inst); //function to handle operands
					 } else if (CallInst *call = dyn_cast<CallInst>(&inst)) {
						 storeFuncCall(*call);//handle function calls
					 } else if (AllocaInst *alloca = dyn_cast<AllocaInst>(&inst)){
						 storeAlloca(*alloca);
					 } else if (GetElementPtrInst *gepi = dyn_cast<GetElementPtrInst>(&inst)) {
						 storeGetElementPtr(*gepi);//handle array access;
					 } else {
						 storeLoadStoreCmp(inst);//function to handle operands
					 }
					 instCounter++;
				}
				bbCounter++;
			}
			BBCountInFun[funcCounter]=bbCounter;
			funcCounter++;
			return true;
		}
		void storeTerminatorOperands(Instruction &inst)
		{
			if(BranchInst *brIns = dyn_cast<BranchInst>(&inst)) {
				if(brIns->isUnconditional()) {
					Value *dest(brIns->getOperand(0));
					VM[funcCounter].operandLabels[instCounter][0]=dest->getName();
					VM[funcCounter].opType[instCounter]=dest->getType();

				} else {
					VM[funcCounter].operandLabels[instCounter][0]=brIns->getOperand(0)->getName();
					VM[funcCounter].operandLabels[instCounter][1]=brIns->getOperand(1)->getName();
					VM[funcCounter].operandLabels[instCounter][2]=brIns->getOperand(2)->getName();
				}
			} else if (ReturnInst *retInst = dyn_cast<ReturnInst>(&inst)) {
				VM[funcCounter].Operands[instCounter][0]=retInst->getReturnValue();
				if(retInst->getReturnValue() == NULL) {
					VM[funcCounter].operandLabels[instCounter][0]= "";//retInst->getReturnValue();
				} else {
					VM[funcCounter].operandLabels[instCounter][0]= retInst->getReturnValue()->getName();
				}
			}
		}
		void storeBinOperands(Instruction &inst) {
			BinaryOperator *binOp = dyn_cast<BinaryOperator>(&inst);
			Value *x(binOp->getOperand(0));
                	Value *y(binOp->getOperand(1));
			VM[funcCounter].nameLabel[instCounter]=inst.getName();
			VM[funcCounter].Operands[instCounter][0]=x;
			VM[funcCounter].Operands[instCounter][1]=y;
			if(x->hasName()) {
			//	errs()<<"x has name.......\n";
				VM[funcCounter].operandLabels[instCounter][0]=x->getName();
			}
			if(y->hasName()) {
			//	errs()<<"y has name.....\n";
				VM[funcCounter].operandLabels[instCounter][1]=y->getName();
			}
			//errs()<<"getOperands are: ....."<< *(binOp->getOperand(0))<<"........"<<*(binOp->getOperand(1))<<"....\n";
			//errs() << "\n \n \n checking whether nsw flag is set..."<< inst.hasNoSignedWrap() << "\n \n \n";
		}
		void storeLoadStoreCmp(Instruction &inst) {
			if(StoreInst *storIns = dyn_cast<StoreInst>(&inst)) {
				if (storIns->getOperand(0)->getName() == "") { //first operand is a value
					VM[funcCounter].Operands[instCounter][0]=storIns->getOperand(0);
				} else {//both operands are refering to previous instructions/variables
					VM[funcCounter].operandLabels[instCounter][0]=storIns->getOperand(0)->getName();
				}
				VM[funcCounter].operandLabels[instCounter][1]=storIns->getOperand(1)->getName();
					
			} else if (CmpInst *cmpIns = dyn_cast<CmpInst>(&inst)){	
				if(cmpIns->getOperand(0)->getName() == "") {
					VM[funcCounter].Operands[instCounter][0]=cmpIns->getOperand(0);
				} else {
					VM[funcCounter].operandLabels[instCounter][0]=cmpIns->getOperand(0)->getName();
				}
				if(cmpIns->getOperand(1)->getName()== "") {
					VM[funcCounter].Operands[instCounter][1]=cmpIns->getOperand(1); //value to compare
				} else {
					VM[funcCounter].operandLabels[instCounter][1]=cmpIns->getOperand(1)->getName();//variable to compare
				}
				VM[funcCounter].nameLabel[instCounter]=cmpIns->getName();
			} else if (LoadInst *load = dyn_cast<LoadInst>(&inst)) {
				VM[funcCounter].operandLabels[instCounter][0]=inst.getOperand(0)->getName();
				VM[funcCounter].opType[instCounter]=inst.getType();
				VM[funcCounter].nameLabel[instCounter]=load->getName();
			}
		}
		void storeFuncCall(CallInst &call) {
			VM[funcCounter].operandLabels[instCounter][0]=call.getOperand(0)->getName();
			VM[funcCounter].opType[instCounter]=call.getType();
			VM[funcCounter].nameLabel[instCounter]=call.getName();
		}
		void storeAlloca(AllocaInst &alloca) {
			errs()<<"........................"<<alloca.getAlignment()<<",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n";
			VM[funcCounter].opType[instCounter]=alloca.getAllocatedType();
			VM[funcCounter].nameLabel[instCounter]=alloca.getName();
		}
		void storeGetElementPtr(GetElementPtrInst &gepi) {
			VM[funcCounter].operandLabels[instCounter][0]=gepi.getOperand(0)->getName();
			VM[funcCounter].Operands[instCounter][1]=gepi.getOperand(1);//i64
			VM[funcCounter].Operands[instCounter][2]=gepi.getOperand(2);//i64
			VM[funcCounter].nameLabel[instCounter]=gepi.getName();
		}
		void globalFunct() {
			errs() << "\n\n Virtualized functions are: \n";
			for(int i=0;i<funcCounter;i++)
			{
				errs()<< Funct[i]<<"\n";
			}
		}
		Constant *newFunct[NUM_FUNS];
		void interpreter() { //function for trying out interpreting VPA and VM of each function
			mod = new Module("test", *C);
		       	for(int i=0; i<funcCounter; i++) {
			        newFunct[i] = mod->getOrInsertFunction(Funct[i],FRetType[i],NULL);
			}	
			for(int i=0; i<funcCounter; i++)
			{
				Function* fun = cast<Function>(newFunct[i]);
				BasicBlock* bbs[BBCountInFun[i]];
				for(int j=0;j<BBCountInFun[i];j++){
					bbs[j]= BasicBlock::Create(*C,BBNames[i][j],fun);
				}
				BasicBlock* block = bbs[0];
				errs()<<"Interpreting function.. "<<insInFunct[i]<<"\n";
				int VPC = 0;

				int bbindex = 0;
				for(int j=0; j<insInFunct[i]; j++)
				{
					auto itr = revRISA.find(VPAS[i][j]);	
					switch (itr->second) {
						case RETURN_INST:{
								handleReturnInstruction(fun,block,i,j);
								break;
							}
						case BRANCH_INST:
							{
								bbindex++;
								handleBranchInstruction(fun,block,bbs,i,j,BBCountInFun[i]);
								block = bbs[bbindex];
								break;
							}
						case INDIRECTBR_INST: 
							{
								break;
							}	
						case ADD_INST:{
								 handleAddInstruction(fun,block,i,j);
								 break;
							 }
						case FADD_INST:{
								       handleFAddInstruction(fun,block,i,j);
								       break;
							       }
						case SUB_INST: {
								       handleSubInstruction(fun,block,i,j);
								       break;
							       }
						case FSUB_INST: {
									handleFSubInstruction(fun,block,i,j);
									break;
								}
						case MUL_INST: {
								       handleMulInstruction(fun,block,i,j);
								       break;
							       }
						case FMUL_INST: {
									handleFMulInstruction(fun,block,i,j);
									break;
								}
						case UDIV_INST: {
									handleUdivInstruction(fun,block,i,j);
									break;
								}
						case SDIV_INST: {
									handleSdivInstruction(fun,block,i,j);
									break;
								}
						case FDIV_INST: {
									handleFdivInstruction(fun,block,i,j);
									break;
								}
						case FCMP_INST: {
									handleFcmpInstruction(fun,block,i,j);
									break;
								}
						case ALLOCA_INST: {
								 handleAllocaInstruction(block,i,j);
								 break;
							}
						case LOAD_INST: {
								 handleLoadInstruction(fun,block,i,j);
								 break;
							 }
						case STORE_INST: {
								 handleStoreInstruction(fun,block,i,j);
								 break;
							 }
						case GEP_INST: {
								 handleGEPInstruction(fun,block,i,j);
								 break;
							 }
						case ICMP_INST: {
								 handleCmpInstruction(fun,block,i,j);
								 break;
							 }
						case CALL_INST: {
								 handleCallInstruction(block,i,j);
								 break;
							 }
						case BITCAST_INST: {
									   handleBitcastInstruction(fun,block,i,j);
									   break;
								   }
						default:
							{
								errs()<<"defaulted \n";
							}
					}
				}
			}
			mod->dump();
		}
		void interpreter2(Module &M) {			
			for(Module::iterator MI=M.begin(); MI !=M.end(); ++MI){
				Function &F(*MI);
				errs() << "\nFunction in interpreter 2: "<<F.getName()<< "\n\n";
			}
		}
		void handleAllocaInstruction(BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			unsigned zero=0;
			Value* null = NULL;
			builder.CreateAlloca(VM[i].opType[j],zero,null,VM[i].nameLabel[j]);
		}
		void handleStoreInstruction(Function* F,BasicBlock* block, int i, int j) {
			IRBuilder<> builder(block);
			Value* op1;
			Value* op2;
			if(VM[i].operandLabels[j][0]!= "") {//first operand is not a constant, so we search all instructions for first and second operand
				for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
					BasicBlock &BB(*FI);
					for(BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi) {
						Instruction &inst(*bbi);
						if(VM[i].operandLabels[j][0] == inst.getName())
						{
							op1 = &inst;
						}
						if(VM[i].operandLabels[j][1] == inst.getName())
						{
							op2 = &inst;
						}
					}
				}
			} else { // first operand is a constant
			op1 = VM[i].Operands[j][0];
				for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
					BasicBlock &BB(*FI);
					for(BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi) {
						Instruction &inst(*bbi);
						if(VM[i].operandLabels[j][1] == inst.getName())
						{
							op2 = &inst;
						}
					}
				}

			}
			builder.CreateStore(op1,op2,false);
		}
		void handleReturnInstruction(Function* F, BasicBlock* block,int i, int j) {
			IRBuilder<> builder(block);
			bool nullFlag= true;
			for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
				BasicBlock &BB(*FI);
				for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){
     					Instruction &inst(*bbi);
					if(VM[i].operandLabels[j][0] != "" && VM[i].operandLabels[j][0] == inst.getName()) {
						nullFlag= false;
						builder.CreateRet(&inst);
					}
				}
			}
			if(nullFlag) {
				builder.CreateRetVoid();
			}
		}
		void handleBranchInstruction(Function* F, BasicBlock* block, BasicBlock* bbs[], int i, int j, int bbcount) {
			IRBuilder<> builder(block);
			bool isUncond = false;
			BasicBlock* True;
			BasicBlock* False;
			Value* Cond;
			for(int k=0; k<bbcount; k++) {
				if(VM[i].operandLabels[j][0] == bbs[k]->getName())
				{
					isUncond = true;
					builder.CreateBr(bbs[k]);
					break;
				}
			}
			if(!isUncond) {

				for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
					BasicBlock &BB(*FI);
				for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){
     					Instruction &inst(*bbi);
					if(CmpInst *cmpIns = dyn_cast<CmpInst>(&inst)) {
						if(cmpIns->getName() == VM[i].operandLabels[j][0]) {
							Cond = cmpIns;
						}
					}
				}
				}
				for(int k=0; k<bbcount; k++) {
					if(VM[i].operandLabels[j][1] == bbs[k]->getName())
					{
						True = bbs[k];
					} else if (VM[i].operandLabels[j][2] == bbs[k]->getName()) {
						False = bbs[k];
					}
				}
				builder.CreateCondBr(Cond,True,False);
			}
		}
		void handleLoadInstruction(Function* F, BasicBlock* block, int i, int j) {
			IRBuilder<> builder(block);

			for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
				BasicBlock &BB(*FI);
				for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){
     					Instruction &inst(*bbi);
					if(AllocaInst *alloca = dyn_cast<AllocaInst>(&inst)) {
						if(alloca->getName() == VM[i].operandLabels[j][0]){
							builder.CreateLoad(VM[i].opType[j],alloca,VM[i].nameLabel[j]);
						}
					}
				}
			}
		}
		void handleCmpInstruction(Function* F, BasicBlock* block, int i, int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
			Value *op2 = VM[i].Operands[j][1];
			for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
				BasicBlock &BB(*FI);
				for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

					Instruction &inst(*bbi);
					if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
						op1= &inst;
					}
					if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
						op2= &inst;
					}
				}
			}
			if(op1 != NULL && op2 != NULL) {
				builder.CreateICmpSGT(op1,op2,VM[i].nameLabel[j]);
			}
		}
		void handleGEPInstruction(Function* F, BasicBlock* block, int i, int j) {
			IRBuilder<> builder(block);

			for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
				BasicBlock &BB(*FI);
			for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){
     				Instruction &inst(*bbi);
				if(AllocaInst *alloca = dyn_cast<AllocaInst>(&inst)) {
					if(alloca->getName() == VM[i].operandLabels[j][0]) {
						Value* args[2] = {VM[i].Operands[j][1], VM[i].Operands[j][2]};
						builder.CreateInBoundsGEP(alloca,ArrayRef<Value *>(args,2),VM[i].nameLabel[j]);
					}
				}
			}
			}
		}
		void handleCallInstruction(BasicBlock* block, int i, int j) {
			IRBuilder<> builder(block);
			for(int k=0; k<funcCounter; k++) {
				if(VM[i].operandLabels[j][0] == newFunct[k]->getName()) {
					CallInst *newCall = builder.CreateCall(newFunct[k]);
					newCall->setName(VM[i].nameLabel[j]);
				}
			}
		}
		void handleAddInstruction(Function* F, BasicBlock* block, int i, int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateAdd(op1,op2,VM[i].nameLabel[j],false,true);
                        }
		}
		void  handleFAddInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateFAdd(op1,op2,VM[i].nameLabel[j]);
                        }

		}
		void  handleSubInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){
                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateSub(op1,op2,VM[i].nameLabel[j],false,true);
                        }

		}
		void  handleFSubInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateFSub(op1,op2,VM[i].nameLabel[j]);
                        }

		}
		void  handleMulInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateMul(op1,op2,VM[i].nameLabel[j],false,true);
                        }

		}
		void  handleFMulInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateFMul(op1,op2,VM[i].nameLabel[j]);
                        }

		}
		void  handleUdivInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateUDiv(op1,op2,VM[i].nameLabel[j],false);
                        }

		}
		void  handleSdivInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateSDiv(op1,op2,VM[i].nameLabel[j],false);
                        }

		}
		void  handleFdivInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateFDiv(op1,op2,VM[i].nameLabel[j]);
                        }

		}
		void  handleFcmpInstruction(Function* F, BasicBlock* block,int i,int j) {
			IRBuilder<> builder(block);
			Value *op1 = VM[i].Operands[j][0];
                        Value *op2 = VM[i].Operands[j][1];
                        for(Function::iterator FI=F->begin();FI != F->end(); ++FI) {
                                BasicBlock &BB(*FI);
                                for (BasicBlock::iterator bbi = BB.begin(); bbi != BB.end(); ++bbi){

                                        Instruction &inst(*bbi);
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][0]) {
                                                op1= &inst;
                                        }
                                        if(inst.getName() != "" && inst.getName() == VM[i].operandLabels[j][1]) {
                                                op2= &inst;
                                        }
                                }
                        }
                        if(op1 != NULL && op2 != NULL) {
                                builder.CreateFCmpOEQ(op1,op2,VM[i].nameLabel[j]);
                        }

		}
		void  handleBitcastInstruction(Function* F, BasicBlock* block,int i,int j) {

		}
	};
char VirtSC::ID = 0;
static RegisterPass<VirtSC> X("VirtSC", "VirtSC Pass");
}

