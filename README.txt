Low Level Virtual Machine (LLVM)
================================

This directory and its subdirectories contain source code for LLVM,
a toolkit for the construction of highly optimized compilers,
optimizers, and runtime environments.

LLVM is open source software. You may freely distribute it under the terms of
the license agreement found in LICENSE.txt.

Please see the documentation provided in docs/ for further
assistance with LLVM, and in particular docs/GettingStarted.rst for getting
started with LLVM and docs/README.txt for an overview of LLVM's
documentation setup.

If you are writing a package for LLVM, see docs/Packaging.rst for our
suggestions.

=======================================
This repo contains implementation of VirtSC 

Initial stage 


To run and test:

mkdir build
cd build
cmake ../<downloaded-folder>
cmake --build .

test:

sample test cases are available in 'testcases' folder

create a test file test.c(put some c code)
clang -O0 -emit-llvm test.c -c -o test.bc
llvm-dis test.bc // to view IR
./bin/opt -instnamer -load ./lib/LLVMVirtSC.so -VirtSC ./test.bc -o test.Virt.bc

instnamer pass :

Gives name to each instruction. 

111914002
Haritha Madhav C


an Example test code that works with current version:

#include<stdio.h>
int foo();
void  main(){
    int a=110;
    int b=220;
    int arr[10];
    int d=8;
    int d5=8;
    int d44=8;
    int d4=8;
    int d3=8;
    int d2=8;
    int d1=8;
    if(d>0) {
	    d=1;
    } else {
	    d=0;
    }
    d=foo();
    arr[1]=d;
}
int foo() {
	int x=123;
	return x;
}

test case II : Arithmetic operations

#include<stdio.h>
int foo();
void  main(){
    int a=110;
    int b=0;
    int d=0;
    float c=2.0;
    float e=3.0;
    if(d>0) {
	    d=1;
	    e=4.0;
    } else {
	    d=0;
	    e=5.0;
    }
    b=foo();
    arr[1]=d+20;
    c=e-c;
}
int foo() {
	int x=123;
	return x;
}



